 import express from 'express';

 import set_place from './set_place.js'


 const app=express();
 const puerto=3000;

 //Ruta: Punto de acceso al aplicativo (interfaz de la API)

 app.get("/greeting", (peticion, respuesta)=>{

    respuesta.send("Hola backend");
 });

 app.get("/set_place", (peticion, respuesta)=>{

    set_place();
    respuesta.send("Algo paso");
    
 });

 //Inicializar el servidor 

 app.listen(puerto,()=>{
    console.log('Servidor escuchando en el puerto 3000');
 });