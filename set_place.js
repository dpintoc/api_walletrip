import mysql from 'mysql'

export default function set_place(){

    //1. Conectarse a la base de datos

    let parametros={

        host:"localhost",
        user: "Diana",
        password: "Root",
        database: "db_walletrip"
    }

    let conexion= mysql.createConnection(parametros);

    //2.Conectarnos a la base de datos

    conexion.connect((err)=>{

        if(err){
            console.log("Error en la conexión a la base de datos");
            console.log(err.message);
            return false;
        }else{
            console.log("Conexión establecida...");
            return true;
        }
    })
    //3. Inserción de los datos
    
    let consulta="INSERT INTO place (idplace, name, description) VALUES (1.'Casa','Casa de empeño')"

    conexion.query(consulta);
    
    //3.Cerrar la conexión a la base de datos 

    conexion.end();
}